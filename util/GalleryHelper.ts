import { GalleryCategory } from "~/ts/interfaces/Gallery";

export class GalleryHelper {
    public static parseCategoryType(value: string): GalleryCategory {
        switch (value) {
            case 'activity':
                return GalleryCategory.ACTIVITY;
            case 'camp':
                return GalleryCategory.CAMP;
            case 'event':
                return GalleryCategory.EVENT;
            case 'course':
                return GalleryCategory.COURSE;
            default:
                return GalleryCategory.ACTIVITY; // Default value if the string doesn't match any category
        }
    }
}