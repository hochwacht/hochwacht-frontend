import type { StrapiMedia } from "~/ts/interfaces/strapi/StrapiMedia";

export class ImageUtil {
    public static getImageFormatURL(image: StrapiMedia, format: "thumbnail" | "large" | "medium" | "small" ): string {
        return image.formats[format]?.url ? image.formats[format].url : image.url 
    }
}