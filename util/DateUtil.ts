import { formatDate } from "date-fns"

export class DateUtil {

    public static formatDate(date: Date | string, format = 'dd.MM.yyyy'): string {
        return formatDate(date, format);
    }
}