// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: true,

  // Add Head Meta Tags
  app: {
    head: {
      title: 'hochwacht.ch - Pfadi Hochwacht Baden',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      htmlAttrs: {
        lang: 'de'
      },
      meta: [
        { hid: 'lang', name: 'lang', content: 'de' },
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Sagen dir Schnitzeljagde, Zeltstädte, Lagerfeuer und Theater zu? Entdecke unter Freunden dein nächstes Abenteuer.' },
        { name: 'format-detection', content: 'telephone=no' },
        {
          property: 'og:title',
          content: `Pfadi Hochwacht Baden`,
        },
        {
          property: 'og:type',
          content: `website`,
        },
        {
          property: 'og:description',
          content: `Pfadi Hochwacht Baden`,
        },
        {
          property: 'og:image',
          content: 'https://test.hochwacht.ch/img/logo-lilie.png',
        },

        { name: 'msapplication-TileColor', content: '#ec7e1e' },
        { name: 'theme-color', content: '#da532c' },
      ],
      link: [
        { rel: 'manifest', href: './manifest.json' },
        // { rel: 'icon', type: 'image/png', href: 'favicon-32x32.png' },
        { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon/apple-touch-icon.png' },
        // { rel: 'stylesheet', href: 'https://unpkg.com/buefy/dist/buefy.min.css' },
      ],
      script: [
        // { src: 'https://unpkg.com/buefy/dist/buefy.min.js' },
      ],
    },
  },

  modules: [
    // 'nuxt-buefy',
    '@nuxtjs/robots',
    '@vite-pwa/nuxt'
  ],

  pwa: {
    /* PWA options */
    filename: 'manifest.json',
  },

  // Add scss

  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/public/scss/main.scss";'
          // additionalData: '@import "@/node_modules/bulma/sass/utilities/_all";'
        }
      }
    }
  },

  router: {
    // 404 page
  },

  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL || 'https://apiv2.hochwacht.ch',
      devApiUrl: process.env.DEV_API_URL || 'http://localhost:1337',
      isTestMode: (process.env.TEST_MODE == 'TEST') || false,
      version: process.env.VERSION || '24.3.1',
      meta: {
        fullTitle : 'hochwacht.ch - Pfadi Hochwacht Baden',
        title: 'Pfadi Hochwacht Baden',
        description: 'Sagen dir Schnitzeljagde, Zeltstädte, Lagerfeuer und Theater zu? Entdecke unter Freunden dein nächstes Abenteuer.',
        image: 'https://hochwacht.ch/img/logo-lilie.png',
      },
    }
  },

  sourcemap: {
    server: true,
    client: true
  },

  compatibilityDate: '2024-09-19'
})