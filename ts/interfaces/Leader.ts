import type { StrapiDataItem } from "./strapi/StrapiData";
import type { StrapiMedia } from "./strapi/StrapiMedia";

export interface LeaderAgegroup extends Leader {}

export interface Leader extends StrapiDataItem {
    firstname: string;
    lastname: string;
    pfadiname: string;
    role: string | null;
    Email: string | null; 
    image: StrapiMedia;

    createdAt: string;
    updated_at: string;
    published_at: string;
}