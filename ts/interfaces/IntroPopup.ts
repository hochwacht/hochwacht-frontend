import type { StrapiDataItem } from "./strapi/StrapiData";

export interface IntroPopup extends StrapiDataItem {
    id: number;
    text: string;
}