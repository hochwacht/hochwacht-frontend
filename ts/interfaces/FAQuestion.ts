import type { RichTextInput } from "./strapi/RichTextBlock";
import type { StrapiDataItem } from "./strapi/StrapiData";

export interface FAQuestion extends StrapiDataItem {
    Question: string;
    Answer: RichTextInput,
    position: number | null,
}