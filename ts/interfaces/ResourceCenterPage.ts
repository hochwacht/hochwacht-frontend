import type { RichTextSection } from "./content/RichTextSection";
import type { StaticComponent } from "./content/StaticComponent";
import type { ContactSection } from "./resources/ContactSection";
import type { DocumentSection } from "./resources/DocumentSection";
import type { LeaderSection } from "./resources/LeaderSection";
import type { FaqComponent } from "./strapi/components/Faq";
import type { StrapiDataItem } from "./strapi/StrapiData";

export interface ResourceCenterPage extends StrapiDataItem {
    id: number;
    Title: string;
    Subtitle: string;
    Url: string;
    Content: ResourceCenterPageContent[];
}

export type ResourceCenterPageContent = RichTextSection | StaticComponent | FaqComponent | LeaderSection | DocumentSection | ContactSection;
