import type { Activity } from "./Activity";
import type { LeaderAgegroup } from "./Leader";
import type { StrapiDataItem } from "./strapi/StrapiData";
import type { StrapiMedia } from "./strapi/StrapiMedia";

export interface Agegroup extends StrapiDataItem {
    id: number;
    title: string;
    name: string;
    description: string;

    quarterly_plans: StrapiMedia;
    group_photo: StrapiMedia;
    registration_form: StrapiMedia;

    activities: Activity[];
    leaders: LeaderAgegroup[];
    Contact: {
        id: number;
        PhoneNumber: string | null;
        Email: string | null;
    } | null;
}