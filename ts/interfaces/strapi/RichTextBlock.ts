import { type Format, type StrapiMedia } from '~/ts/interfaces/strapi/StrapiMedia'
// NODES
export interface TextNode {
    text: string;
    type: 'text';
    bold?: boolean;
    underline?: boolean;
    italic?: boolean;
    strikethrough?: boolean;
    code?: boolean;
}

export interface LinkNode {
    url: string;
    type: 'link';
    children: TextNode[];
}

export interface ListItemNode {
    type: 'list-item';
    children: (TextNode | LinkNode)[];
}

// BLOCKS
export interface ParagraphBlock {
    type: 'paragraph';
    children: (TextNode | LinkNode)[];
}

export interface HeadingBlock {
    type: 'heading';
    level: number;
    children: (TextNode | LinkNode)[];
}

export interface ListBlock {
    type: 'list';
    format: 'unordered' | 'ordered';
    children: ListItemNode[];
}

export interface QuoteBlock {
    type: 'quote';
    children: (TextNode | LinkNode)[];
}

export interface CodeBlock {
    type: 'code';
    // Just one TextNode in the Children
    children: [TextNode]
}

export interface ImageBlock {
    type: 'image';
    image: StrapiMedia;
    // children: TextNode[] is always an empty string
}

export type BlockNode = ParagraphBlock | HeadingBlock | ListBlock | QuoteBlock | CodeBlock | ImageBlock;


export type RichTextInput = BlockNode[];