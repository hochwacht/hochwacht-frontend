export interface StrapiDataItem {
    id: number;
    documentId: string;
    created_at: string;
    updated_at: string;
    locale: string | null;
}

export interface StrapiMeta {
    pagination: {
        page: number;
        pageSize: number;
        pageCount: number;
        total: number;
    }
}

export interface StrapiData<T> {
    data: T | null;
}

export interface StrapiDataCollection<T> {
    data: T[];
    meta: StrapiMeta;
}