import type { RichTextInput } from "../RichTextBlock";

export interface FaqComponent {
    id: number;
    __component: "component.faq";
    Questions: FaqQuestion[];
}

export interface FaqQuestion {
    id: number;
    Question: string;
    Answer: RichTextInput;
}