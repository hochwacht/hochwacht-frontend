export interface Contact {
    id: number;
    PhoneNumber?: string;
    Email?: string;
    Name?: string;
}