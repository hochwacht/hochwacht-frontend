import type { Leader } from "./Leader";
import type { RichTextInput } from "./strapi/RichTextBlock";
import type { StrapiDataItem } from "./strapi/StrapiData";
import type { Contact } from "./strapi/components/Contact";

export interface Department extends StrapiDataItem {
    Title: string;
    Description: RichTextInput;
    Contact: Contact;
    leaders: Leader[];
}