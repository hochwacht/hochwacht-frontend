import type { StrapiDataItem } from "./strapi/StrapiData";

export interface Activity extends StrapiDataItem {
    id: number;
    name: string;
    start_date: string;
    end_date: string;
    location: string;
    remarks: string;

}