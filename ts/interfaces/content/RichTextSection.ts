import type { RichTextInput } from "../strapi/RichTextBlock";

export interface RichTextSection {
    id: number;
    __component: "layout.section";
    title: string;
    content: RichTextInput;
}