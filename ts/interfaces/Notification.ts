import type { StrapiDataItem } from "./strapi/StrapiData";

export enum NotificationLevel {
    ERROR = 'error',
    WARNING = 'warn',
    INFO = 'info',
    SUCCESS = 'success'
}
export interface Notification extends StrapiDataItem {
    id: number;
    title: string;
    content: string;
    level: NotificationLevel | null;
    enabled: boolean;
}