import type { Contact } from "../strapi/components/Contact";

export interface ContactSection {
    id: number;
    __component: "resource-center.contact-section";
    Column: boolean;
    Contacts: Contact[];
}