import type { StrapiMedia } from "../strapi/StrapiMedia";

export interface DocumentSection {
    id: number;
    __component: "resource-center.document-section";
    Title: string;
    DisplayMode: "list" | "card" | "button";
    Documents: {
        id: number;
        Title: string;
        Document: StrapiMedia;
    }[];
}