import type { Leader } from "../Leader";

export interface LeaderSection {
    id: number;
    __component: "resource-center.leader-section"
    Column: boolean
    leaders: Leader[]
}