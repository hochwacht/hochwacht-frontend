import type { StrapiDataItem } from "./strapi/StrapiData";
import type { StrapiMedia } from "./strapi/StrapiMedia";

export interface Document extends StrapiDataItem {
    description: string;
    is_flyer: boolean;
    file: StrapiMedia;
    thumbnail: StrapiMedia;
}
